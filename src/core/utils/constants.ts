export const messageConstants = {
  userErrors: {
    CANNOT_CREATE: 'User could not be created',
    INVALID_CREDENTIALS: 'Invalid credentials',
    USER_NOT_FOUND: 'No such user found',
  },
};
