import { ObjectType, Field } from '@nestjs/graphql';
import { User } from './user.entity';

@ObjectType()
export class Response {
  @Field()
  status: boolean;
  @Field()
  message: string;
}

@ObjectType()
export class LoginResponse {
  @Field()
  access_token: string;
  @Field()
  details: User;
}
