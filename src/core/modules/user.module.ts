import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { UserService } from 'src/core/services';
import { UserMutation } from '../resolvers';
import { User, UserSchema } from '../schemas';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: User.name,
        schema: UserSchema,
      },
    ]),
  ],
  providers: [UserService, UserMutation],
  exports: [UserService, UserMutation],
})
export class UserModule {}
