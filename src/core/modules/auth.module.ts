import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { ConfigModule } from '@nestjs/config';
import { AuthService } from 'src/core/services';
import { AuthMutation } from 'src/core/resolvers';
import { UserModule } from './user.module';

@Module({
  imports: [
    UserModule,
    JwtModule.register({
      secret: 'secretValue',
      signOptions: { expiresIn: '60000s' },
    }),
    ConfigModule.forRoot(),
  ],
  providers: [AuthService, AuthMutation],
  exports: [AuthService, AuthMutation],
})
export class AuthModule {}
