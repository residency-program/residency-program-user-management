import { InputType, Field } from '@nestjs/graphql';

@InputType()
export class CreateUserInput {
  @Field()
  email: string;
  @Field()
  password: string;
  @Field()
  firstName: string;
  @Field()
  lastName: string;
}

@InputType()
export class LogInUserInput {
  @Field()
  email: string;
  @Field()
  password: string;
}

@InputType()
export class GetUserInput {
  @Field()
  email: string;
  @Field()
  password: string;
}
