import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { User, UserDocument } from 'src/core/schemas';
import { CreateUserInput } from 'src/core/dto';

@Injectable()
export class UserService {
  constructor(@InjectModel(User.name) private userModel: Model<UserDocument>) {}

  async createUser(createUserInput: CreateUserInput) {
    try {
      const email = createUserInput.email.toLowerCase();
      const isUserExist = await this.userModel.find({ email: email });

      if (isUserExist.length === 0) {
        const user = new this.userModel(createUserInput);
        const isUserCreated = await user.save();
        if (isUserCreated) {
          return { status: true, message: 'User created successfully.' };
        }
      } else {
        return {
          status: false,
          message: 'User already exists.',
        };
      }
    } catch (err) {
      return {
        status: false,
        message: 'User could not be created! Please try again.',
        error: err,
      };
    }
  }

  async getUserByEmail(email: string): Promise<User> {
    const user = await this.userModel.findOne({ email: email });
    return user;
  }
}
