import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UserService } from '../user';

@Injectable()
export class AuthService {
  constructor(
    private userService: UserService,
    private jwtService: JwtService,
  ) {}

  async login(email: string, password: string) {
    try {
      const userDetails = this.validateUser(email, password);
      if (userDetails) {
        const payload = { username: email, sub: password };
        const token = this.jwtService.sign(payload);
        return {
          access_token: token,
          details: userDetails,
        };
      }
    } catch (err) {
      throw err;
    }
  }

  async validateUser(email: string, password: string): Promise<any> {
    try {
      const user = await this.userService.getUserByEmail(email);
      if (user && user.password === password) {
        const { _id, firstName, lastName, email } = user;
        const details = {
          _id: _id,
          firstName: firstName,
          lastName: lastName,
          email: email,
        };
        return details;
      }
      return `Could not validate user`;
    } catch (err) {
      throw err;
    }
  }
}
