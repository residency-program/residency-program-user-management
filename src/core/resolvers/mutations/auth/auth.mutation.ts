import { Resolver, Args, Query } from '@nestjs/graphql';
import { User, LoginResponse } from 'src/core/entities';
import { AuthService } from 'src/core/services';

@Resolver(() => User)
export class AuthMutation {
  constructor(private readonly authService: AuthService) {}

  @Query(() => LoginResponse, { name: 'LogIn' })
  async logInUser(
    @Args('email', { type: () => String }) email: string,
    @Args('password', { type: () => String }) password: string,
  ) {
    try {
      const result = await this.authService.login(email, password);
      return result;
    } catch (err) {
      return {
        message: 'Invalid credentials',
        err,
      };
    }
  }
}
