import { Resolver, Mutation, Args } from '@nestjs/graphql';
import { HttpException, HttpStatus } from '@nestjs/common';
import { User, Response } from 'src/core/entities';
import { UserService } from 'src/core/services';
import { messageConstants } from 'src/core/utils';
import { CreateUserInput } from 'src/core/dto';

@Resolver(() => User)
export class UserMutation {
  constructor(private readonly userService: UserService) {}

  @Mutation(() => Response, { name: 'CreateUser' })
  async createUser(@Args('CreateUserInput') createUserInput: CreateUserInput) {
    try {
      const response = await this.userService.createUser(createUserInput);
      return response;
    } catch (err) {
      throw new HttpException(
        {
          status: HttpStatus.UNPROCESSABLE_ENTITY,
          error: messageConstants.userErrors.CANNOT_CREATE,
        },
        HttpStatus.FORBIDDEN,
      );
    }
  }
}
